---
title: "Gymnastique d'entretien"
---

La gymnastique douce (ou d’entretien) désigne une **activité corporelle basée sur des exercices physiques destinés à assouplir ou à développer le corps**. C’est un mélange de mouvements propres au fitness, au stretching ou au yoga. Elle met en jeu l’ensemble de l’appareil locomoteur de la personne et respecte son anatomie fonctionnelle.

La gymnastique douce est un concept alliant étirements, contractions musculaires, méthodes de respiration et automassages. Elle repose sur plusieurs principes : 

- **L’accompagnement** : les mouvements se font toujours en étant à l’écoute de son corps. Le geste doit se faire « tout seul », on se laisse conduire par ses sensations de façon à progresser en douceur, sans forcer et sans risque de se faire mal.

- **La profondeur** : le mouvement trouve son origine au plus profond du corps. Afin d’être stable au cours des exercices et d’acquérir par la suite une certaine sérénité dans la vie quotidienne, la personne doit en effet puiser sa force relativement profondément.

- **La lenteur** : les mouvements doivent être exécutés sans précipitation et avec de la lenteur afin de bien prendre conscience des sensations.

- **La globalité** : toutes les articulations du corps sont sollicitées au cours des mouvements qui se font ainsi amples et structurés.

- **La souplesse** : la gym douce a pour but de rendre sa souplesse au corps ainsi qu’à l’esprit.

- **Le tonus** : les mouvements sont effectués avec lenteur et relâchement, ce qui confère au corps force et tonus. En effet, contrairement aux idées communément répandues, la vitalité ne s’acquière pas avec des mouvements rapides et succincts, mais plutôt avec des exercices lents et profonds.

- **La respiration** : la respiration est très importante car elle stimule la circulation de l’énergie.

- **L’unité** : l’idée est d’unifier le corps et l’esprit par des mouvements structurés et réfléchis.

La gymnastique douce est valable pour tous, quel que soit l’âge, le sexe ou la condition physique : elle s’adresse aussi bien aux seniors qu’aux handicapés. Cette activité propose une respiration par le ventre permettant une élimination facile des déchets et l’évacuation des tensions négatives. 

La gym douce n’a aucune contre-indication et possède de multiples bienfaits. Elle est notamment efficace contre le stress, la fatigue chronique, l’insomnie, les problèmes digestifs et/ou cardiovasculaires, les douleurs articulaires, le mal de dos… C’est enfin une excellente méthode de relaxation apportant un bien-être intégral. A raison de 1 heure par semaine au saint de notre association, cette activité présente des bénéfices notoires pour la santé.

-----------------
Source : http://www.st-jacut-les-pins.fr/images/associations/gym/LesBonnesRaisons.pdf

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

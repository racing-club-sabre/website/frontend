---
title: 'Escrime'
---

Respect et Maîtrise de soi sont les maîtres mots de l'escrime. Sport de combat, il n'est en aucun cas basé sur la force. L'escrime est une discipline complète qui demande de la vitesse, de la précision et des réflexes. Sport individuel mais impossible sans partenaire, il se pratique aussi en équipe.

L'escrime est un Sport Olympique Français Majeur, avec 115 médailles olympiques au compteur… Laura Flessel Colovic, Brice Guyart, Eric Srecki, Christian d'Oriola sont les escrimeurs les plus titrés et les plus connus des sportifs français, sans oublier Jean-François Lamour deux fois médaillé d’or aux jeux olympiques au sabre, arme que nous pratiquons dans notre club.
C'est un sport en expansion. La <abbr title="Fédération Française d'Escrime">FFE</abbr> compte 64 000 licenciés, 120 000 pratiquants, et plus de 800 clubs. Il n'existe aucune contrainte médicale pour pratiquer ce sport. L'équipement est léger et la pratique très sécurisée. De plus, grâce à des Maîtres d'Armes qualifiés, vous maîtriserez rapidement la technique et la coordination des gestes.

L'escrime développe aussi bien la concentration que la réflexion. Pour harmoniser au mieux le corps et l'esprit !
Libérez-vous... Le jeu est à la pointe de l'épée et vous permet de vous mesurer à vos adversaires dans la plus grande convivialité. Affinez-vous... Idéal pour l'entretien musculaire, l'escrime vous tonifie et affine votre silhouette : élégance et ligne feront partie de votre quotidien !

## Sport pour les enfants

L’escrime est une activité extra-scolaire qui a la cote ! Il existe en France plus de 800 clubs d’escrime et les enfants peuvent pratiquer ce sport dès 5 ans. Zoom sur ce loisir ouvert à tous.

### L’escrime dès 5 ans

L’escrime est un sport de combat qui s’enseigne en club au niveau départemental et qui est rattaché à la Fédération Française d’Escrime. Pour les plus jeunes, dès 5 ans, cette activité sportive est proposée sous forme de cours d’ « éveil-escrime » à un niveau d’initiation.

### L’escrime pour tous

La pédagogie est adaptée aux bambins et elle est entièrement construite autour de l’intérêt des jeunes pratiquants. Tous les enfants peuvent pratiquer l’escrime, il ne faut pas posséder d’aptitudes spécifiques. Tous débutent l’escrime au même niveau.
L’escrime développe chez les enfants la vitesse dans l'exécution des gestes, la précision, une bonne coordination, et au final, une tonicité du côté musculaire.

### Je découvre les valeurs de l’escrime :

*   Je suis poli
*   J’ai l’esprit sportif
*   Je respecte l’autre
*   J’aide l’autre
*   Je suis persévérant
*   Je suis attentif
*   Je me maîtrise
*   J’ai le goût de l’effort


L’escrime a des vertus éducatives pour l’enfant. Elle permet de l'épanouir sur le plan physique et mental, et le prépare à développer 3 qualités physiques maîtresses que sont l’observation, la vivacité de jugement et l’esprit de décision.
Elle offre aussi aux adolescents et aux adultes selon leurs goûts, un excellent dérivatif physique et intellectuel, ou l’attrait de nombreuses compétitions de différents niveaux. C’est également un exercice et un sport parfaitement adaptés à la femme pour qui la pratique est possible sans préjudice lié à la puissance physique.

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

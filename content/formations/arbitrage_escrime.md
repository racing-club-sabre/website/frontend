---
title: "Arbitrage à l'escrime"
---

Pour pratiquer l'escrime en compétitions **il faut être trois** : deux tireurs et un arbitre.

**Arbitrer, c'est diriger le combat**. L'escrime est l'un des très rares sports où le français est la langue officielle. Chaque pays utilise sa langue pour les compétitions nationales, mais dès que la compétition devient internationale, le français est obligatoire pour l'arbitrage. « _En Garde ! Prêts ? Allez ! Halte !_ ... ». L'arbitre dispose en plus d'un code de signe pour expliquer chaque phrase d'armes.

Le combat entre deux escrimeurs, aussi appelé assaut ou match selon que l'on compte ou non les points, est constitué de « phrases d'armes ». Une phrase d'armes s'apparente à une discussion entre deux personnes où chacune à son tour s'exprime à l'aide de son arme. L'arbitre « écoute » et analyse cette phrase d'armes pour décider s'il attribue un point à l'un des deux escrimeurs. Selon l'arme, il applique la règle appropriée : attribution du point au premier qui touche à l'épée ou à celui qui touche en ayant la priorité au fleuret et au sabre.

L'arbitre joint le geste à la parole pour décrire les échanges et annoncer sa décision aux escrimeurs et au public. Il dirige le combat à l'aide de commandements : _En garde – Prêt – Allez_.
Dès qu'une touche est portée ou qu'un des deux escrimeurs commet une faute, il arrête l'échange en disant _Halte !_ L'arbitre énonce la phrase d'armes en utilisant un vocabulaire et une gestuelle spécifiques. Il décrit ainsi les actions réalisées par les deux escrimeurs afin de déterminer si le point compte ou non.

De plus il fait respecter les règles du jeu et de sécurité et sanctionne, à l'aide de cartons, toutes les fautes de combat qu'il constate. **Jaune : avertissement/ Rouge : touche de pénalité/ Noir : expulsion.**

L'arbitre officialise l'issue du duel en désignant son vainqueur. Tout le long d'un match, l'arbitre est aidé par des assesseurs ou juges de main. Aujourd'hui, il a également recours à l'outil vidéo pour l'aider dans sa décision.

------------
Source : Fédération française d'escrime. Tous les règlements et documents à ce sujet sont à retrouver [sur ce lien](https://www.escrime-ffe.fr/fr/arbitrage/reglements-et-documents.html). 

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Quart de finale Belledonne"
date: '2022-01-23'
author: 'Tristan'
cover: '/img/blog/article13-head.jpg'
---
**Retour sur les performances de nos tireurs la semaine dernière à Villard-Bonnot ! 🌟**

Des grandes premières, une famille habituée, et de l’expérience engrangée : _que d’émotions_ pour les tireurs du RCS la semaine dernière lors des quarts de finale des championnats de France 🤩🤩

👉 En seniors, **mention spéciale pour Manon Bruyat** qui, encore une fois, monte sur la plus haute marche du podium et ramène _une coupe en plus à la maison_ 🏆. Et elle est évidemment accompagnée de sa petite soeur, Constance, qui termine 2ème en M15 ! Félicitations à toute la famille 🥰

👉 **En M17 chez les filles**, Julie Denet termine à la 6ème place, tandis que Arthur Félix (18ème) et Yan Boutarin (19ème) représentaient eux aussi le club chez les garçons. 

👉 **Pour les M15 filles**, Constance récupère donc une belle médaille d’argent 🥈! Elle est suivie de près par Maëlle Bouchet (5ème), et Agathe Mege (7ème) qui viennent donc placer 3️⃣ tireuses du RCS dans le top 8️⃣. Après elles, Méryl Gaydamour termine 14ème, Anouk Bouchet 16ème, Maëlys Defrenne 20ème et enfin Soaliah Cros 23ème. 

👉 **Chez les M15 garçons**, c’est Théo Guichard qui se classe le mieux avec sa 12ème place. Nino Besançon (15ème), Baptiste Raucy (18ème), Ethan Thivolle (27ème) et Sascha Zawadzki (32ème) complètent les résultats du RCS. 

👉 Enfin, **pour les plus petits**, il faut aussi féliciter la superbe 1ère place de Méline Porcel en M10, ainsi que les 2ème et 3ème places de Swan Alin et Mathieu Menet en M11 ! Une nouvelle génération promise à un bel avenir 😍🔥

A noter d’ailleurs la qualification de la quasi-totalité des tireurs pour les demi-finales des championnats de France, qui se dérouleront à Saint-Jean de la Ruelle 💪

A très vite sur les pistes ! 🤺

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre/posts/3209793705941891

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

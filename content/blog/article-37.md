---
title: "1/2 finale Championnats de France M15"
date: '2023-04-02'
author: 'Tristan'
cover: '/img/blog/article37-head.jpg'
---
Suite de Besançon, les -15 ans continuaient l’aventure le week-end dernier en traversant la France jusqu'à Brest en Bretagne ! 🥹

👉 Grosse perf pour Maëlle Bouchet qui termine à une belle 11ème place ! Elle est suivie de ses compères Agathe Mège (28ème), Lisa Rey-Fayolle (34ème), puis Lou Brugère (42ème) et enfin Anouk Bouchet (61ème). 

👉 Chez les garçons, la meilleure performance revient à Arthur Asmus, qui intègre le tableau de 64 avec sa 61ème position. Il est suivi de Baptiste Raucy (69ème) et Vincent Bruyère (78ème). 

Félicitations à nos tireurs (qui pour la plupart sont issus de l’Ensemble Scolaire Saint Louis), et restez connectés pour la suite 😉

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Stage de la Toussaint"
date: '2022-10-25'
author: 'Tristan'
cover: '/img/blog/article29-head.jpg'
---
🚨 Record battu pour un stage organisé par le club ! 🚨

31 tireurs inscrits au gymnase Chareyre, rien que ça… rien de mieux pour passer à nouveau un excellent moment et travailler dur dans le but d’atteindre de bons résultats cette saison 💪

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Stage de la Toussaint"
date: '2023-10-27'
author: 'Tristan'
cover: '/img/blog/article50-head.jpg'
---
Retour en images sur le dernier stage de la Toussaint ! Beaucoup de tireurs et des bons moments assurés 🤗

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


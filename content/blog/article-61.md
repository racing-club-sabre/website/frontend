---
title: "Séjour Sabre & Surf en Sardaigne"
date: '2024-04-20'
author: 'Tristan'
cover: '/img/blog/article61-head.jpg'
---
Vivez un séjour de rêve Escrime & Surf en Sardaigne 🤺🏄

Rendez-vous du 12 au 24 juillet 2024 pour cette expérience hors du commun, avec Naturel Séjours ! Perfectionnez votre pratique du sabre avec le Maître Del Monaco et (re)découvrez les bienfaits des Sports Nautiques 😎

⏩️ Bonus : avec la formule parrainage, bénéficiez de 20% de réduction pour vous-même et la personne parrainée ! 

Plus d’informations sur naturelsejours.com 💻 ou par téléphone au 06 12 09 35 42 📲

Plus d'informations à retrouver ici 👉 https://www.youtube.com/watch?v=jbTu-kS_Em0

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

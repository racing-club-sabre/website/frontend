---
title: "M15 & M17 Meylan"
date: '2023-10-08'
author: 'Tristan'
cover: '/img/blog/article48-head.jpg'
---
C’est reparti ! Les M17 et M15 étaient à Meylan pour lancer leur saison, et déjà quelques breloques ont été ramenées à la maison grâce aux filles ! 🤩

👉 En M17, ce sont effectivement Constance Bruyat (🥈ème), Maëlle Bouchet (🥉ème) et Agathe Mège (🥉ème) qui s’approprient le podium quasi à elles seules ! Il ne manque plus que l’or… 👀
Elles sont suivies par Lou Brugère (8ème), Margot Porcel (10ème), Méryl Gaydamour (17ème) et Lison Porcel (18ème). 

👉 Chez les garçons en M17, Arthur Asmus (surclassé) termine à la 10ème place, suivi par Nino Besançon, 14ème. 

👉 En M15, Baptiste Raucy termine 7ème, juste devant Arthur Asmus (8ème). Suivent Hector Nougaret (16ème), Sasha Zawadzki (17ème), Soan Alin (19ème), Matyeu Menet (22ème), Arthur Durand (24ème), Adam Cherif el Farissy (30ème), Adam Tavares (31ème), Celian Luguin (32ème), Tom Azzolin (34ème) et Tom Caqué (35ème). 

👉 Enfin, chez les filles toujours en M15, Agathe Mège termine 5ème, Anouk Bouchet 7ème, Lou Brugère 8ème, Lison Porcel 12ème, Lisa Rey—Fayolle 13ème, Méline Porcel 14ème, Alma Pannetier 15ème, Hélène Ostermann 19ème, Elisa Brun 20ème, et enfin Jeanne Deman-Lemaître termine à la 23ème place. 

Première sortie en force donc pour les tireurs du RCS Escrime (notamment grâce aux élèves de l’Ensemble Scolaire Saint-Louis) dont de nombreux néophytes découvraient la compétition ! 🆕

Rendez-vous très rapidement pour les premiers circuits nationaux de la saison en M20 et M17 🤺

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Championnats de France M17"
date: '2024-05-21'
author: 'Tristan'
cover: '/img/blog/article64-head.jpg'
---
**Alors les filles ?**

👉 Constance conclut parfaitement sa saison en M17 en terminant à une belle 16ème place en National 1. 

👉 Après de gros efforts au long de l'année, Agathe parvient à se qualifier en National 1 en surclassement et finit 29ème. Bravo à elle 👏

👉 Maëlle et Lou tiraient toutes les deux en National 2 et terminent respectivement à la 17ème et 20ème place. 

Petite pensée à Margot qui n'a malheureusement pas pu tirer à Vannes ! 


**Alors les garçons ? **

Arthur et Baptiste ont enfilé leur tenue ce samedi 18 mai afin de tester leurs limites chez les M17. Voici les résultats de leur surclassement🤺

Les deux tireurs du RCS ont combattu toute la journée en National 2. Après de bonnes poules, Arthur s'est finalement imposé à la 40ème place. Baptiste ferme la marche en terminant 62ème. De bons résultats pour nos petits M15 😉

Après cette expérience en surclassement, la véritable étape finale se déroulera pour eux en juin lors des Championnats de France de leur catégorie (M15). 


**Par équipes **

À cette occasion, le RCS a envoyé son équipe féminine composée de Constance Bruyat, Agathe Mège, Maëlle Bouchet et Lou Brugère. 

On peut féliciter cette magnifique équipe pour nous avoir rapporté une médaille de bronze en finissant 3ème en National 2 👏

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

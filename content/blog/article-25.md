---
title: "Stage national de Vichy -15 ans"
date: '2022-08-05'
author: 'Tristan'
cover: '/img/blog/article25-head.jpg'
---
Patrick était présent pour encadrer **le stage national de Vichy des -15 ans** 🏆🇫🇷

Entre le 15 et le 20 juillet, les 14 meilleurs français du classement national de cette catégorie se sont retrouvés en Auvergne pour préparer l’avenir. 

Notre maître d’armes, aux côtés de l’entraîneur national du Pôle France Relève, a guidé ces jeunes tireurs à travers différents tests physiques et psychologiques, dans le but de détecter les futures pépites de l’escrime française ✨

Au terme de ces 5 jours intensifs, un bilan global était envoyé aux maîtres d’armes respectifs de ces jeunes talents, avec les points forts et les points faibles à améliorer. 🤝

👏 Nous sommes fiers que Patrick ait été appelé pour réaliser cette analyse et encadrer ce stage ! 

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


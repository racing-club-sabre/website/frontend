---
title: "Challenge des mousquetaires"
date: '2022-04-10'
author: 'Tristan'
cover: '/img/blog/article17-head.jpg'
---

Pluie de médailles d’argent au challenge des Mousquetaires de Corbas, où nos M11 et M13 étaient venus en nombre pour réaliser ces superbes performances ! 🥈

👉 Chez les garçons en M13, Baptiste Raucy obtient une superbe 2ème place ! Il est suivi par Arthur Asmus (10ème), Sascha Zawadzki (16ème), Kayan Bruno (18ème), et Ethan Thivolle (24ème)

👉 Chez les filles en M13, Agathe Mege, Soaliah Cros et Anouk Bouchet squattent le podium et terminent respectivement 2ème et 3ème ex-aequo ! 

👉 Pour les M11, copié collé filles-garçons : Soan Alin termine 2nd, suivi par Adam Cherif El Farissy (5ème) ; tandis que pour les filles Lisa Rey-Fayolle monte elle aussi sur le podium et prend la 2ème place, et est suivie par Méline Porcel (5ème). 

Bravo à toutes et tous pour cette belle compétition 🥰

Toutes les photos à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

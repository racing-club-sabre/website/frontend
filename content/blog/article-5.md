---
title: "Open M13 Lyon"
date: '2021-10-03'
author: 'Tristan L'
cover: '/img/blog/article5-head.jpg'
---
Quelle performance ! 😁

Superbe journée pour nos jeunes tireurs à Lyon samedi dernier ! Cet open -13 ans a été l’occasion pour de nombreuses jeunes pousses de venir découvrir les joies de la compétition et des médailles gagnées !

Chez les garçons, **c’est Kayan Bruno qui, pour sa première compétition, monte sur la plus haute marche du podium** après un magnifique parcours. Un grand bravo à lui !

![Bruno Kayan médaillé d'or et les autres tireurs du RCS avec leurs maîtres d'armes](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article5-text.jpg)

S’en suivent Ethan Thivolle qui prend la 6ème place, puis Baptiste Raucy 9ème, Sascha Zawadzki 10ème, Arthur Asmus 14ème et enfin Soan Alin en 19ème position. Chez les filles, Inaya Lambert, Maëlys Defrenne et Soaliah Cros engrangent aussi les médailles et terminent respectivement 2ème, 3ème et 4ème. De très bon augure pour la suite ! 

Merci aux organisateurs et bravo aux compétiteurs pour cette belle journée de fête !

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Régional V4 Tassin"
date: '2024-02-11'
author: 'Tristan'
cover: '/img/blog/article58-head.jpg'
---
Chez les vétérans, François Amanrich continue d’épater ! Lors du circuit régional V4 à Tassin, il monte sur la seconde marche du podium et confirme sa belle forme 👏🥈

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Stage vacances de Pâques"
date: '2022-04-25'
author: 'Tristan'
cover: '/img/blog/article15-head.jpg'
---

Quelques nouvelles du stage des vacances de Pâques ! 

Près de 25 tireurs étaient réunis au gymnase Chareyre, pour se préparer aux championnats de France qui se profilent en cette fin de saison. 

Travail tactique, perfectionnement technique et assauts étaient au programme, avec nos chers maîtres d’arme tout juste rentrés de leur stage régional à Voiron. Ambiance studieuse, mais joyeuse ! 😍

📸 Merci à Marcial Cordero (Dauphiné Libéré) pour les photos, à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Interview projet salle d'armes"
date: '2021-12-15'
author: 'Tristan L'
cover: '/img/blog/article11-head.jpg'
---
Nouvelle salle d’armes à Crest… 

Le projet de complexe sportif dédié à l’escrime et en partenariat avec l’Ensemble Scolaire Saint Louis continue son petit bonhomme de chemin en prévision des jeux de Paris 2024 !

Futur lieu d’entraînement pour le club et pour la section sport-étude, le financement est bouclé à 90% ! Sponsors, dons ou mécénats, [nous n’attendons que vous](https://jaidemonecole.org/campaigns/449-saint-louis-drome-crest-projet-olympique-paralympique) pour la pose de la première pierre dans quelques mois… restez connectés ! 

Pour en savoir plus, retrouvez l’interview de Patrick Del Monaco et de M. Moucadel, chef d’établissement de Saint Louis, [au micro de France Bleu Drôme Ardèche](https://www.francebleu.fr/emissions/100-sports/drome-ardeche/100-sport-268). 

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

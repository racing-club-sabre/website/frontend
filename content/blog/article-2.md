---
title: "Du jeu... aux Jeux !"
date: '2021-09-06'
author: 'Tristan L'
cover: '/img/blog/article2-head.jpg'
---
**Du jeu... aux Jeux !**

C’est la reprise ! Après une année compliquée en raison de la pandémie, le **Racing Club Sabre** est ravi d’accueillir de nouveau ses tireurs à la salle d’armes pour une nouvelle saison qui s’annonce magnifique, après les Jeux Olympiques de Tokyo. 

L’escrime nous a effectivement fait rêver cet été : lors de cette olympiade, ce ne sont pas moins de 5 médailles qui sont venues s’ajouter au tableau des médailles françaises. Manon Brunet et l’équipe féminine au sabre, Romain Canonne à l’épée, ainsi que les équipes de fleuret masculine et féminine, ont participé à la fête et ont permis à la France de se classer comme la 2ème nation mondiale de ce sport historique, après le Comité olympique russe. 

![Communication FFE](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article2-text.png)

Un mois plus tard, les salles d’armes rouvrent leurs portes pour les petits comme pour les grands, et cela concerne bien sûr également le RCS. Pour fêter ce nouveau départ, un [stage de reprise](https://www.facebook.com/racingclubsabre/posts/3093176114270318) organisé par les maîtres d’armes du club a fait redécouvrir les joies du sport et le bien-être que cela procure à ses jeunes tireurs. 

Présent dans trois villes différentes (Crest, Livron, Valence), le RCS propose deux séances gratuites d'essai lors du mois de septembre. Pour toute demande d’information concernant les inscriptions ou l’organisation de cette année, contacter le maître Patrick Del Monaco au 06 87 08 87 14.

Pour adhérer directement, c'est en suivant [ce lien](https://www.helloasso.com/associations/racing-club-de-sabre/adhesions/racing-club-de-sabre) ! 


______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. 
Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

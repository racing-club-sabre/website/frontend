---
title: "C'est la reprise !"
date: '2021-09-03'
author: 'Tristan L'
cover: '/img/blog/article1-head.jpg'
---
**C’est la reprise !**

*ENFIN* ! Après une année compliquée en raison de la pandémie, le Racing Club Sabre est ravi de vous accueillir de nouveau à la salle d’armes pour une nouvelle saison pleine d’émotions et de surprises ! 

Le sport vous a manqué et les JO de cet été vous ont fait rêver ? Nous aussi ! Rejoignez l’aventure et participez aux rêves olympiques en intégrant notre club d’escrime, pour peut-être un jour viser les étoiles comme Manon Brunet, Enzo Lefort, Romain Cannone et tant d’autres… 

Pour fêter ce nouveau départ, [le stage d'escrime](https://www.facebook.com/racingclubsabre/posts/3093176114270318) organisé par nos maîtres d’armes il y a quelques jours nous a fait redécouvrir les joies du sport et le bien-être que cela procure, pour le plus grand plaisir de nos jeunes tireurs !

![Photo de groupe du stage](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article1-texte.jpg)

Pour toute demande d’information concernant les inscriptions ou l’organisation de cette année, n’hésitez pas à contacter Patrick Del Monaco au 06 87 08 87 14.

Pour adhérer directement, c'est en suivant [ce lien](https://www.helloasso.com/associations/racing-club-de-sabre/adhesions/racing-club-de-sabre) ! 


______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

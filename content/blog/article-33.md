---
title: "Championnat régional M11 & M13"
date: '2022-12-11'
author: 'Tristan'
cover: '/img/blog/article33-head.jpg'
---
Nos jeunes ont de l’avenir ! 🤩🤩

Les M11 et les M13 se retrouvaient à Villard Bonnot pour leurs championnats régionaux respectifs, et n’ont pas fait dans la dentelle 😏

👉 C’est un triplé pour les filles dans la catégorie moins de 11 ans, puisque Méline Porcel, Hélène Ostermann et Clémence Raucy s’approprient les trois places sur le podium 🥇🥈🥉

👉 Chez les garçons en moins de 11 ans, Adam Cherif El Farissy récupère une magnifique médaille d’argent 🥈, et est suivi par Pierre Emmanuel Cartigny (6ème), Célestin Lecoq (9ème) et Malo Thonon Sturbois (12ème). 

👉 Pour les M13 filles, c’est Anouk Bouchet qui remporte la compétition 🥇 ! Elle est suivie par Lisa Rey-Fayolle (7ème) et Elisa Brun (9ème). 

👉 Enfin, chez les M13 garçons, Mathyeu Menet termine lui aussi 2ème 🥈, devant Soan Alin (10ème) et Adam Tavares. 

Félicitations à toutes et tous 🥳

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


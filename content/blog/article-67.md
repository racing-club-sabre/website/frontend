---
title: "Championnats de France M20"
date: '2024-06-03'
author: 'Tristan'
cover: '/img/blog/article67-head.jpg'
---
Patrick a accompagné Constance, Yan, et Arthur à la finale des Championnats de France M20 à Charenton ce samedi 1er juin. 🤺

Yan et Arthur ont chacun entamé leur compétition avec 2 victoires et 3 défaites en poule, ce qui les a classés respectivement 49ème et 51ème avant les tableaux. Leur bon classement leur a permis d'être exemptés pour le premier tour. Malheureusement, ils ont tous deux été éliminés au second tour : Arthur par le 14ème et Yan par le 16ème. Yan a terminé 50ème et Arthur 52ème dans cette finale des Championnats de France en National 2.

Constance a bien commencé sa compétition avec 3 victoires et seulement 2 défaites, ce qui lui a permis de se classer 9ème avant les tableaux. Elle a ensuite remporté ses matchs 15 à 12 contre la 24ème du classement, puis 15 à 14 contre la 8ème. Après un très bon parcours, elle s'est inclinée 15 à 13 contre la première du classement et a terminé à une honorable 7ème place en National 2 en étant surclassée.

Bravo à tous les trois !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

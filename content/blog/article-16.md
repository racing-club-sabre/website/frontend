---
title: "Challenge René Queyroux"
date: '2022-04-04'
author: 'Tristan'
cover: '/img/blog/article16-head.jpg'
---

Retour sur la performance de François Amanrich au challenge international René Queyroux à Lyon 👀

Auteur d’un superbe tableau, il s’impose sans trembler 15-4 en finale et s’adjuge la première place 🥇

Félicitations à lui 😍👏

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

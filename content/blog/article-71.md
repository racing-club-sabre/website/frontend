---
title: "Stage national Vichy"
date: '2024-06-28'
author: 'Tristan'
cover: '/img/blog/article71-head.jpg'
---
Bonne nouvelle !

Agathe MÈGE et Baptiste RAUCY ont tous les deux été sélectionnés pour le stage national à Vichy !

En effet, nos deux tireurs issus de la section sportive de Saint Louis ont été repérés grâce à leurs excellents résultats en finale de la Fête des Jeunes. Agathe (3ème) et Baptiste (16ème) se rendront donc à Vichy du 9 au 15 juillet. 🤺

Bravo à tous les deux ! 👏

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Fête des Jeunes 2023"
date: '2023-06-11'
author: 'Tristan'
cover: '/img/blog/article44-head.jpg'
---
Retour sur la Fête des Jeunes, les championnats de France -15 ans, qui avaient lieu à Albi 🤺

Jolie performance de nos tireurs, et surtout de nos tireuses, présentes en force pour cette dernière compétition de l’année 😎

👉 Maëlle Bouchet arrive 19ème au classement final, suivie par Lou Brugère, 21ème, et Agathe Mège, 26ème, qui atteint elle aussi les 16èmes de finale. Un peu plus loin, Lisa Rey—Fayolle termine 50ème, suivie par Soaliah Cros (57ème), Lison Porcel (59ème) et enfin Anouk Bouchet (60ème). 

👉 Chez les garçons, Vincent Bruyère termine lui à la 76ème place. 

👉 Enfin, par équipe le dimanche, Maëlle et Agathe terminent sur la troisième marche du podium national en faisant partie de l’équipe 1 Auvergne Rhône-Alpes 🥉🔥

Bravo à toutes et tous ! et à très vite pour une rentrée qui va vite arriver… 🙃

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Quart de finale M15 Besançon"
date: '2024-02-04'
author: 'Tristan'
cover: '/img/blog/article56-head.jpg'
---
Un immense bravo à Agathe Mège qui monte sur la troisième marche du podium lors du quart de finale des championnats de France M15, qui avaient lieu à Besançon 🔥🥉

Une excellente performance pour elle à quelques mois de la Fête des Jeunes, qui laisse présager une très belle fin de saison ! Bravo à elle 😍

👉 Chez les filles, Agathe est ensuite suivie par Anouk Bouchet (44ème), Lison Porcel (45ème), Lou Brugère (62ème), Méline Porcel (72ème), Jeanne Deman-Lemaître (83ème), Alma Pannetier (105ème), Hélène Ostermann (107ème) et Elisa Brun (110ème). 

👉 Chez les garçons, la meilleure performance revient à Arthur Asmus, qui termine à la 21ème place. Il est suivi de près par Baptiste Raucy, qui atteint lui aussi le tableau de 32 et termine à la 23ème place. Derrière eux, on retrouve Hector Nougaret (45ème), Adam Tavares (158ème), Yanis Azzolin (171ème), et enfin Arthur Durand (184ème). Soan Alin a lui été contraint d’abandonner sur blessure. 🤕

Bravo à vous toutes et tous ! Et merci aux accompagnateurs 😉

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

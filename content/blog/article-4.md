---
title: "Circuit national Orléans M17"
date: '2021-09-26'
author: 'Tristan L'
cover: '/img/blog/article4-head.jpg'
---
Circuit national Orléans M17 🤺

Enfin ! Après plus d’un an sans compétition, le Racing Club Sabre se déplaçait de nouveau en terres orléanaises pour le premier circuit national -17 ans de l’année, dans la grande salle d’armes du club et ses 24 pistes.

![Manon Moccelin et Julie Denet pendant la compétition](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article4-text.jpg)

Les garçons absents, ce sont bien deux crestoises qui représentaient nos couleurs ce weekend pour cette reprise : **Manon Moccelin termine à la 48ème** place tandis que **Julie Denet récupère la 56ème place** pour cette première de l’année. Une marge de progression évidente et des repères à retrouver, mais surtout un plaisir de reprendre la compétition pour nos jeunes ! 

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

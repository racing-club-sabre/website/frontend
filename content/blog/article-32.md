---
title: "Circuit national M17 à Saint-Jean de la Ruelle"
date: '2022-12-03'
author: 'Tristan'
cover: '/img/blog/article32-head.jpg'
---
Les M17 de nouveau de sortie pour le circuit national de Saint-Jean de la Ruelle, lors d’une compétition individuelle et par équipe ! 🤺

👉 Chez les garçons, très belle perf’ de Yan Boutarin qui termine 43ème ! Il est suivi par Arthur Félix (89ème), Vincent Bruyère (95ème) et Théo Guichard (108ème). 

👉 Chez les filles, Constance Bruyat échoue aux portes des seizièmes de finale et doit se contenter de la 33ème place. Derrière elle, trois autres filles qui ont aussi atteint le tableau de 64 : Margot Porcel (47ème), Agathe Mège (60ème) et Maëlle Bouchet (62ème). 

👉 Par équipe, les garçons accrochent une superbe 7ème place, tandis que les filles terminent 15èmes. 

Félicitations à toutes et tous ! 😍

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


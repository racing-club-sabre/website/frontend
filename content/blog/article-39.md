---
title: "Circuit national M17 Géménos"
date: '2023-04-24'
author: 'Tristan'
cover: '/img/blog/article39-head.jpg'
---
Retour aux choses sérieuses pour les M17 ! 👊

Direction le sud et Géménos dans la banlieue de Marseille pour nos jeunes lors d’un week-end de compétition individuelle et par équipes ☀

👉 Résultat homogène chez les filles avec la 31ème place de Constance Bruyat, qui est suivie par Agathe Mège (34ème), Margot Porcel (35ème) et Maëlle Bouchet (38ème). 

👉 Chez les garçons, la meilleure performance revient à Vincent Bruyère qui termine 49ème, suivi par Arthur Félix, 57ème, qui a lui aussi parvenu à entrer dans le tableau de 64. Ils sont suivis par Baptiste Raucy (78ème), Yan Boutarin (79ème) et enfin Arthur Asmus (93ème). 

👉 Pour la compétition par équipes du dimanche, les filles terminent à une belle 8ème place, tandis que l’équipe 1 des garçons composée de Yan, Arthur Félix et Vincent finit 19ème, et l’équipe 2 avec Arthur Asmus, Baptiste et Théo Guichard les suivent à la 20ème position. 

Bravo à eux ! Restez connectés pour une fin de saison qui s’annonce palpitante 😉

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Circuit Elite Gisors M20"
date: '2021-10-10'
author: 'Tristan L'
cover: '/img/blog/article6-head.jpg'
---
Circuit Elite Gisors M20 🤺

Pour les -20 ans également, ce weekend signifiait le retour sur les pistes ! 

Antonin Roche et Manon Bruyat profitaient de ce weekend en Normandie pour regoûter aux joies de la compétition dans une nouvelle catégorie qu’ils découvraient après tout ce temps sans compétition. 

![Weekend sympathique pour nos M20 en compagnie des maîtres d'armes et du Président](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article6-text.jpg)

De quoi se remettre dans le bain et signer des performances honorables pour un retour ! Antonin s’incline en tableau de 64 sur un score serré contre un cador et termine 57ème, tandis que Manon échoue aussi aux portes des 16èmes de finale après des poules équilibrées et accroche la 35ème place. La saison est lancée, il faudra compter sur eux pour les prochaines échéances !

Spéciale dédicace à Caliste Fregonese du club de Rillieux qui nous a accompagné sur ce sympa weekend ! 

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

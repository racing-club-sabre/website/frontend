---
title: "Fête des Jeunes 2024"
date: '2024-06-18'
author: 'Tristan'
cover: '/img/blog/article68-head.jpg'
---
Le RCS de Crest ramène une médaille de bronze aux Championnats de France M15 ! 🥉

Ce week-end, pendant les journées du 15 et 16 juin, s'est déroulée la compétition que tous les moins de 15 ans attendent : la Fête des Jeunes, l'occasion de clôturer au mieux leur saison. 🤺

Nos tireurs du club d’escrime de Crest (RCS) ont obtenu de très bons résultats pour cette finale des Championnats de France M15 : 

Chez les filles, Agathe MEGE réalise une performance exceptionnelle en se positionnant comme la troisième meilleure tireuse française de sa catégorie en individuel. Lou BRUGERE finit 12ème lors de cette compétition d’envergure nationale. Après ces deux têtes de liste, c’est Lisa REY-FAYOLLE qui réalise une belle performance en finissant 44ème en étant surclassée. Elle est suivie de près par Lison PORCEL 53ème, Anouk BOUCHET 55ème et Elisa BRUN 75ème. Méline PORCEL et Alma PANNETIER, toutes les deux en surclassement, ont terminé respectivement 80 et 100ème. 

Chez les garçons c’est Arthur ASMUS qui réalise la meilleure performance. Après avoir perdu en quart de finale, il termine à une très belle 8ème place et entre dans les 10 meilleurs français. Baptiste RAUCY fait également une bonne compétition en terminant 18ème à cette finale des Championnats de France. Hector ferme la marche avec une honorable 72ème place. 

Chez les équipes, les résultats sont également très bons. Pour les championnats de France M15, la Fédération a décidé de faire s’affronter des équipes par ligue. Chez les filles Agathe MEGE était dans l’équipe 1 de la ligue AURA (Auvergne Rhône-Alpes) et a terminé en tant que deuxième meilleure équipe française. Lou et Anouk étaient toutes les deux en équipe 2 AURA et sont tombées contre l’équipe d’Agathe en demi-finale, elles terminent à une magnifique 3ème place !  

Et enfin, Lison, Lisa, et Méline étaient toutes les trois en équipe 3 AURA finissent 3ème en National 3. 

Chez les garçons, Arthur et Baptiste faisaient tous les deux partie de l’équipe 1 homme de la ligue AURA et finissent à une très belle deuxième place en National 2. 

Bravo à tous et à toutes pour cette belle fin de saison !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

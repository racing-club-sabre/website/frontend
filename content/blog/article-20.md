---
title: "Challenge de Varces"
date: '2022-05-15'
author: 'Tristan'
cover: '/img/blog/article20-head.jpg'
---
Pluie de médailles pour le [RCS Escrime](https://www.facebook.com/racingclubsabre) au **Challenge de Varces** 😍👏

Ce week-end, nos M15 et nos jeunes M11 se sont déplacés en Isère pour un challenge international de sabre, et ont de nouveau brillé ✨

👉 Chez les filles en -15 ans, la troisième de la fratrie Bruyat décroche une superbe 2ème place ! Bravo à toi Constance 🥰
Elle est suivie de près par Maëlle Bouchet qui complète le podium (3ème), puis par Agathe Mege (6ème), Inaya Lambert (7ème), Méryl Gaydamour (8ème), Leïla Spaak (15ème), Soaliah Cros (16ème) et Anouk Bouchet (17ème). **5 filles dans le top 8** 🔥

👉 Chez les garçons toujours en -15 ans, un trio s’est formé aux portes des quarts de finale avec Théo Guichard qui termine 9ème, puis Vincent Bruyère 10ème et Nino Besançon, 11ème. Ils sont suivis par Ethan Thivolle (19ème) et Sascha Zawadzki (24ème). 👊

👉 Pour les -11 ans, Soan Alin termine 1er chez les garçons, tandis que chez les filles Méline Porcel (2ème) et Lisa Rey-Fayolle (3ème) complètent les couleurs du podium 🥇🥈🥉

Une très belle performance et un très beau moment pour nos jeunes tireurs, qui permettent au RCS, de fait, de remporter la **première place au classement des clubs** 🚀

Félicitation à toutes et tous 🤩

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

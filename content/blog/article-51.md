---
title: "Zone M15 & M17 Annecy"
date: '2023-11-12'
author: 'Tristan'
cover: '/img/blog/article51-head.jpg'
---
Les résultats du zone M17 et M15 d’Annecy de novembre ! 🏔️

👉 En M17, Constance Bruyat récupère la médaille d’argent 🥈 en terminant 2ème et était accompagnée sur le podium par Agathe Mège, 3ème 🥉 ! 
Elles sont suivies par Lou Brugère (10ème), Margot Porcel (13ème) et Lison Porcel (15ème). 

👉 Chez les garçons en M17, le RCS était représenté par Baptiste Raucy (14ème) et Arthus Asmus (18ème), tous les deux surclassés !

👉 Le lendemain, en M15, les mêmes Baptiste et Arthur terminent respectivement à la 8ème et 13ème place. Ils sont suivis par Hector Nougaret (20ème), Mathieu Menet (30ème), Sasha Zawadski (39ème), Yanis Azzolin (44ème), Célian Luguin (48ème), Adam Cherif El Farissy (50ème), Adam Tavares (52ème) et enfin Arthur Durand (54ème). 

👉 Enfin, pour les filles en M15, c’est Anouk Bouchet qui réalise le meilleur résultat en terminant à la 5ème position, suivie de près par Agathe Mège, 6ème. Suivent ensuite Lison Porcel (10ème), Lou Brugère (13ème), Lisa Rey-Fayolle (16ème), Méline Porcel (19ème), Hélène Ostermann (20ème), Alma Pannetier (21ème), Elisa Brun (24ème), et Jeanne Deman-Lemaître (28ème). 

Avec, encore une fois, une large partie de ces tireurs provenant de l’Ensemble scolaire Saint-Louis 🤝

Bravo à toutes et tous ! 

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Régional Géménos"
date: '2024-01-21'
author: 'Tristan'
cover: '/img/blog/article55-head.jpg'
---
Direction le sud et Gémenos pour le RCS, lors du régional des M11, M13, M17 et M20 ! ☀️

👉 En M11 chez les filles, Clémence Raucy termine à une superbe 🥈ème place ! Elle est suivie par Agathe Dubois Morel (8ème), Yland Fabbro (9ème), Rose Ferrari (10ème) et Swane Défausse (11ème). 

👉 Chez les garçons, Younes Bouguerra monte aussi sur le podium avec sa 🥉ème place ! Pierre-Emmanuel Cartigny (9ème), Loukas César Besançon (16ème), Rodrigo Pereira Maciel (18ème) et Noah Mahieu (22ème) suivent. 

👉 En M13, Méline Porcel récupère également la 🥈ème place, suivie de près par Hélène Ostermann (🥉ème) ! Elles sont suivies par Constance Lanfray (8ème), Alma Pannetier (9ème) et Lili Rose Gache (15ème). 

👉 Pour les garçons, Soan Alin réalise la meilleure performance en terminant 7ème. Après lui, on retrouve Arthur Durand (14ème), puis Adam Cherif El Farissy (18ème), Tom Caqué (22ème) et Malo Thonon Sturbois (25ème). 

👉 En M17, Agathe Mège et Maëlle Bouchet terminent sur le podium elles aussi (🥈ème et 🥉ème) ! Constance Bruyat termine cette fois-ci 9ème, suivie par Lou Brugère (14ème), Margot Porcel (15ème) et enfin Lison Porcel (20ème). 

👉 Chez les hommes, le meilleur résultat revient à Arthur Asmus (13ème). Nino Besançon (21ème) et Baptiste Raucy (23ème) complètent ces résultats. 

👉 Enfin, en M20, Constance Bruyat se rattrape et signe une belle 6ème place ! Chez les garçons, Arthur Félix termine 16ème et Yan Boutarin, lui, 22ème. 

Il ne manque maintenant plus que la plus haute marche du podium ! Bravo à tous nos tireurs 😊

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "1/4 de finale Championnats de France M15"
date: '2023-01-22'
author: 'Tristan'
cover: '/img/blog/article36-head.jpg'
---
Les -15 ans étaient à Besançon pour les quarts de finale des Championnats de France 🇫🇷, et c’est Maëlle Bouchet qui signe la meilleure performance avec une belle médaille glanée grâce à sa 7ème position ! 🏅👏

👉 Elle est suivie par Agathe Mège (17ème), Sohalia Cros (34ème), Maëlys Defrenne (51ème), Lou Brugère (54ème), Anouk Bouchet (65ème), Lison Porcel (76ème), Lisa Rey—Fayolle (83ème), Malia Froment (88ème), et enfin Elisa Brun (104ème). 

👉 Chez les garçons, Vincent Bruyère accroche un joli seizième de finale et termine 32ème de la compétition. Après lui, Baptiste Raucy termine 92ème, Hector Nougaret 143ème, Sacha Zawadzki 149ème, et Arthur Asmus 151ème. 

Bravo à eux pour cette compétition de grosse envergure ! On croit en eux pour la prochaine étape 🔜😎

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


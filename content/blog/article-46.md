---
title: "Stage de pré-rentrée"
date: '2023-08-30'
author: 'Tristan'
cover: '/img/blog/article46-head.jpg'
---
Les escrimeurs du RCS Escrime sont bien rentrés ! 🤺

Retour en photos sur le stage de pré-rentrée avec 22 tireuses et tireurs présents pour reprendre doucement leurs marques en ce début de saison 🤗

Le lancement d’une saison qui s’annonce pleine d’émotions 😍

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Coupe d'Europe vétérans"
date: '2024-05-07'
author: 'Tristan'
cover: '/img/blog/article63-head.jpg'
---
François Amanrich (3ème en partant de la gauche) fait ses preuves à l’international et n’aura pas chômé cette saison !

Il concrétise ses ambitions en se hissant à la 3ème place lors de la coupe d’Europe Vétéran 4 se déroulant à Athènes !
👏🌍🎖️

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

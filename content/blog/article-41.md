---
title: "François 21ème aux Championnats d'Europe"
date: '2023-05-18'
author: 'Tristan'
cover: '/img/blog/article41-head.jpg'
---
François Amanrich, 21ème du Championnat d’Europe Vétérans à Thionville ! Un immense bravo à lui 🤩👏

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


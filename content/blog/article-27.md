---
title: "Partenariat avec la Caisse d'Epargne au château de Suze-la-Rousse"
date: '2022-09-30'
author: 'Tristan'
cover: '/img/blog/article27-head.jpg'
---
Le Racing Club Sabre et l’Ensemble scolaire Saint-Louis au château de Suze-la-Rousse pour la signature d’un partenariat avec la Caisse d’Epargne Loire Drôme Ardèche ✍

👉 C’est ce lieu chargé d’histoire qui a été le théâtre de l’accord entre ces entités pour le soutien de la réalisation de la future salle d’armes, prévue pour 2024 !

Visite guidée du château, démonstration de tir à l’arc, d’escrime historique et moderne… et de superbes clichés à la clef 😍📸

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


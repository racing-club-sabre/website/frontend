---
title: "Challenge de Varces"
date: '2023-05-21'
author: 'Tristan'
cover: '/img/blog/article42-head.jpg'
---
Les filles raflent tout sur leur passage au Challenge de Varces ! 🤯🔥

Le RCS Escrime était une nouvelle fois superbement représenté pour cette compétition iséroise dédiée aux M9, M11 et M15 ! Et c’est une pluie de médailles qui vient s’abattre, spécialement pour les filles ♀

Tous les résultats : 

👉 Pour les moins de 9 ans chez les filles, Agathe Dubois Morel remporte la compétition, suivie par Jeanne Astre (3ème) 🥇🥉

👉 Chez les moins de 11 ans, toujours chez les filles, victoire de Méline Porcel, suivie par Clémence Raucy (3ème) et Hélène Ostermann (5ème) 🥇🥉

👉 Chez les garçons, en moins de 11 ans, Pierre-Emmanuel Cartigny finit à la 9ème place, suivi par Malo Thonon Sturbois (15ème) et Amaury Nassim (24ème). 

👉 Pour nos adolescentes, en moins de 15 ans (et toutes de l’Ensemble Scolaire Saint-Louis), Agathe Mège monte sur le podium avec sa 3ème place, et elle est suivie par Lisa Rey-Fayolle (6ème), Lou Brugère (7ème), et Malia Froment (20ème) 🥉

👉 Enfin, chez nos M15 garçons, c’est Baptiste Raucy qui signe la meilleure performance en terminant 14ème. Suivent Arthur Asmus (27ème), Adam Tavares (33ème) et Mathyeu Menet. 

Félicitations à tous ces jeunes qui défendent avec succès nos couleurs 💛🖤 De belles années les attendent 🤩

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


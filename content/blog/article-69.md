---
title: "Agathe Mège 3e française M15 !"
date: '2024-06-19'
author: 'Tristan'
cover: '/img/blog/article69-head.jpg'
---
Agathe réalise une performance exceptionnelle aux Championnats de France M15 !!

Pour cette fin de saison, Agathe s'est rendue à la Fête des Jeunes avec un seul objectif en tête : faire de son mieux pour atteindre le podium des meilleures françaises. Et c'est chose faite !

Après avoir brillé pendant cette saison 2023-2024, Agathe s'illustre parfaitement à Mâcon en tant que 3ème meilleure tireuse française de sa catégorie.🤺🥉

Retrouvez prochainement une interview dédiée à son parcours et ses ambitions. 😉

Bravo Agathe !! 

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

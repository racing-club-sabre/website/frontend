---
title: "Stage de la Toussaint"
date: '2021-11-07'
author: 'Tristan L'
cover: '/img/blog/article9-head.jpg'
---
Stage de la Toussaint 🤺

Fin des vacances aujourd’hui ! L’occasion de revenir sur le super stage de la Toussaint qui a eu lieu la semaine passée, avec pas moins de 24 tireurs de tous les âges venus s’exercer.

Entre rires, travail et bonne humeur, nos jeunes ont pu profiter de l’expérience de leurs maîtres d’armes ainsi que de tireurs plus expérimentés : on est sûr qu’ils vivront les mêmes émotions ! 

Et, en prime, l’occasion de souhaiter un bon anniversaire à Nino Besançon, qui fêtait ses 14 ans !

![Les jeunes tireurs du RCS pendant le stage](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article9-text.jpg)

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

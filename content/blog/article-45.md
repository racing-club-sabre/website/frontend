---
title: "Rentrée des classes 23-24 !"
date: '2023-08-22'
author: 'Tristan'
cover: '/img/blog/article45-head.jpg'
---
👨‍🏫 Le RCS Escrime fait sa rentrée ! Retrouvez-nous aux dates suivantes pour découvrir l’escrime et vous inscrire, vous ou vos enfants, pour la saison qui arrive 📝

👉 Le 30/08 de 14h à 18h : portes ouvertes à la salle d’armes de Crest (Quai Berangier de la Blache)
👉 Le 02/09 : forum des associations de Livron 
👉 Le 03/09 : forum des associations de Crest 
👉 Le 03/09 : forum des associations de Beaumont-lès-Valence 

Nous avons hâte de vous retrouver et/ou de vous rencontrer ! 🤗

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


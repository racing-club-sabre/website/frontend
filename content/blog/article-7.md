---
title: "Horizon 2032 AuRA M15"
date: '2021-10-23'
author: 'Tristan L'
cover: '/img/blog/article7-head.png'
---
Horizon 2032 AuRA pour nos -15 ans 🤺

Retour sur les pistes pour nos jeunes tireurs ce week-end du côté des montagnes de Meylan pour une compétition régionale M15 ! 

Le RCS se déplaçait en nombre en terres grenobloises avec de nombreux tireurs surclassés tirant d’habitude en -13 ans. De quoi engranger déjà de premières expériences à un niveau un peu plus élevé et voir ce qui les attend pour les années à venir.

On note chez les filles la superbe 2ème place de Constance Bruyat qui, comme ses soeurs avant elle, continue de faire briller sur la piste la famille de notre cher Président en montant également sur les podiums ! 

![Large groupe pour le RCS à Meylan](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article7-text.jpg)

Pour les autres résultats toujours chez les filles, Méryl Gaydamour récupère une belle 5ème place, suivie par Maëlle Bouchet 8ème, Aghate Mege 9ème, Inaya Lambert 11ème, Maëlys Defrenne 12ème, Leïla Spaak 15ème et enfin Soaliah Cros 17ème. 

Pour les garçons, pour la plupart M13, c’est Vincent Bruyère qui réalise la meilleure compétition en terminant 11ème. Suivent Nino Besançon à la 13ème place, Baptiste Raucy à la 21ème, Sascha Zawadzki 22ème, Ethan Thivolle 23ème, Arthur Asmus 25ème, et enfin Kayan Bruno 29ème. 

A très vite sur les pistes !
______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

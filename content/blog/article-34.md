---
title: "Eurosabre 2023"
date: '2023-01-15'
author: 'Tristan'
cover: '/img/blog/article34-head.jpg'
---
Le Racing Club Sabre était ce week-end à Meylan pour l’Eurosabre, première Coupe d’Europe des -17 ans de la saison ! ✨

Le haut niveau, les jeunes du RCS le connaissent depuis déjà quelque temps ; mais le très haut niveau, c’était une première pour certains d’entre eux ce week-end : apprentissage compliqué mais instructif pour les jeunes de Crest-Livron, qui ont montré de belles choses lors de cette compétition internationale ! 👊

👉 Chez les filles, le meilleur résultat revient à Constance Bruyat qui termine 83ème de la compétition. Elle est suivie par Maëlle Bouchet (109ème), puis Agathe Mege (112ème) et enfin Margot Porcel (132ème). 

👉 Chez les garçons, il n’étaient que deux : Yan Boutarin termine 130ème, et Vincent Bruyère 163ème. 

Bravo à eux ! 👏

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Championnats de France UGSEL"
date: '2022-05-20'
author: 'Tristan'
cover: '/img/blog/article21-head.jpg'
---
Quelques nouvelles des championnats de France UGSEL ! 😎

Le [Racing Club Sabre](https://www.facebook.com/racingclubsabre) et ses tireurs de l’[Ensemble Scolaire Saint-Louis](https://www.facebook.com/EnsembleScolaireSaintLouis) étaient (très) nombreux à se déplacer à Toulouse, et ont à nouveau ramené d’excellents résultats, pour faire briller le club et l’école au niveau national 🤩✨

👉 En catégorie promotionnelle benjamine (débutants cette saison), les jeunes pousses ont fait fureur et ramènent les plus belles breloques possibles à la maison ! 🔥
Félicitations à Adam Tavares chez les garçons qui remporte la médaille d’or ! Il est suivi par Clément Bonini (6ème), Leandro Puaux (9ème), Yanis Azzolin (10ème) et Clément Brun (11ème). 
Même démonstration chez les filles avec Lou Brugère qui monte elle aussi sur la plus haute marche du podium, suivi par Lana Delplancs (3ème), puis Mya Blanc (7ème), Malia Froment (8ème), Jeanne Deman-Lemaître (10ème) et Elisa Brun (14ème). 
D’excellents éléments qui découvraient la compétition pour la première fois, et qui seront pleinement intégrés au club l’an prochain 😁👏

👉 En catégorie benjamin élite, Arthur Asmus décroche une belle 3ème place et est suivi de près par son camarade Baptiste Raucy (4ème), puis par Sascha Zawadzki (6ème) et Kayan Bruno (11ème). 
Chez les filles benjamines en élite, ce fut une razzia : Agathe Mege (1ère), Soaliah Cros (2ème) et Inaya Lambert (3ème) s’accaparent le podium, et sont suivies par Maëlys Defrenne (5ème) et Lisa Rey-Fayolle (6ème) 🤯

👉 Du côté des minimes en élite, Vincent Bruyère et Constance Bruyat montent aussi sur le podium (3èmes) et sont suivis par Zachari Coin chez les garçons et Méryl Gaydamour chez les filles, à la septième place. 💪

👉 Enfin, en cadet élite, Yan Boutarin, 3ème, et Arthur Félix, 5ème, complètent les excellents résultats de nos tireurs. 

Une grande fierté pour le club et pour Saint-Louis, qui prouve une nouvelle fois la solidité du projet commun de sport-études entre les deux entités ! Et ce n’est que le début 🥰

A très vite sur les pistes 🤺

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

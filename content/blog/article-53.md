---
title: "Coupe d'Europe Eislingen"
date: '2023-12-09'
author: 'Tristan'
cover: '/img/blog/article53-head.jpg'
---
Focus sur Constance Bruyat qui était à Eislingen en Allemagne en décembre pour sa première Coupe d’Europe à l’étranger ! 🌍

Après de très bonnes poules (4 victoires ✅, 2 défaites ❌), une entorse l’empêche de performer autant qu’elle le souhaitait, et elle termine à la 91ème place. 🤕

Ce n’est que partie remise 👊

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


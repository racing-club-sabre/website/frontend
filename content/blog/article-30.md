---
title: "Circuit national M17 Tarbes"
date: '2022-11-06'
author: 'Tristan'
cover: '/img/blog/article30-head.jpg'
---
La saison démarre sur les chapeaux de roue pour les M17 ! 😎

Présents à Tarbes pour un circuit national des moins de 17 ans, nos jeunes s’en sont très bien sortis pour cette première de la saison 👏

👉 Les résultats chez les garçons : Vincent Bruyère accède aux 16ème de finale et termine à une superbe 30ème place 😁 Corentin Veillard termine à la 84ème position, et il est suivi de près par Yan Boutarin, 87ème. Baptiste Raucy ferme la marche à la 117ème place. 

👉 Chez les filles, c’est une confirmation du niveau homogène et compétitif entre elles : Constance Bruyat termine elle aussi 30ème, et elle est suivie Maëlle Bouchet (39ème), Agathe Mège (40ème) et Méryl Gaydamour (41ème) - quel trio ! Enfin, Margot Porcel prend la 53ème place. 🙌

Bravo à toutes et tous pour cette belle compétition, qui annonce une chose : il faudra compter sur le RCS cette saison 💪

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


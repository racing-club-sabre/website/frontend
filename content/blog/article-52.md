---
title: "Régional M9, M13 & M20"
date: '2023-12-03'
author: 'Tristan'
cover: '/img/blog/article52-head.jpg'
---
Début décembre, les M9, M13 et M20 étaient de sortie pour la compétition régionale et ont décroché un nombre incalculable de médailles 🤩🏅

👉 En M20, bravo à Yan Boutarin qui monte sur la boîte grâce à sa 3ème place 🥉 ! Il est suivi par Arthur Félix, 6ème, puis par Corentin Veillard (8ème), Nino Besançon (10ème), et Théo Guichard (11ème). 

👉 En M13, chez les filles, on ne laisse filer que la première place ! Lisa Rey-Fayolle termine deuxième 🥈, et est accompagnée par Hélène Ostermann et Méline Porcel à la troisième place 🥉
Elles sont suivies par Louise Doitrand (7ème), Lili-Rose Gache (8ème), Alma Pannetier (9ème), Constance Lanfrey (10ème) et Clémence Raucy (16ème). 

👉 Chez les garçons, Soan Alin termine 2ème 🥈 ! Suivent Adam Cherif El Farissy (8ème), Mathyeu Menet (13ème), Leo Jules Reymond (14ème), Arthur Durand (15ème), Tom Caque (16ème), Malo Thonon Sturbois (19ème), Younes Bouguerra (21ème), Pierre Emmanuel Cartigny (22ème), et Célestin Lecoq (24ème). 

👉 Enfin, en M9, félicitations à Youmna Bouguerra qui remporte la compétition chez les filles 🥇 ! Chez les garçons, Maxence Ranchon récupère la médaille de bronze 🥉, et est suivi par Amaury Gillerond (5ème) et Manoé Artal (6ème). Des graines de champions ! 

Un grand bravo à toute cette belle équipe, dont certains découvraient pour la première fois les joies de la compétition 🥰

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


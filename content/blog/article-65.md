---
title: "Challenge de Varces"
date: '2024-05-30'
author: 'Tristan'
cover: '/img/blog/article65-head.jpg'
---
Le RCS élu meilleur club du challenge de Varces !! 🤺

Le RCS a reçu ce prestigieux trophée en reconnaissance de ses 4 podiums et de la participation exceptionnelle de ses tireurs.

Voici les résultats impressionnants de nos filles, omniprésentes en tête du classement 🤺

En catégorie M9, Youmna a décroché la médaille d'or en terminant première. 🥇 Bravo à elle !

En catégorie M15, Agathe a réalisé une performance exceptionnelle en terminant deuxième. 🥈 Elle est suivie de près par ses coéquipières du RCS : Lou finit 5ème, Lison 6ème, Anouk 7ème, Hélène 8ème, et Méline, surclassée, termine 12ème.

Pour finir, Clémence a réussi à monter sur la troisième marche du podium en catégorie M11. 🥉 Agathe et Noémie se sont classées respectivement 8ème et 10ème.

Bravo à toutes les filles pour leurs performances exceptionnelles !


Les garçons aussi se sont très bien battus et ont rapporté quelques médailles au club. 😉

Voici les résultats de nos garçons du RCS 🤺

En catégorie M15, Baptiste s'est hissé à la troisième place du podium. 🥉 Il est suivi par ses coéquipiers :

- Arthur ASMUS 10ème 

- Hector NOUGARET 23ème 

- Yanis AZZOLIN 25ème 

- Soan ALIN 29ème 

- Adam TAVARES 34ème 

- Adam CHERRYF EL FARISSY 35ème 

- Célian LUGUIN 36ème 

- Arthur DURAND 43ème 

En catégorie M11, Pierre Emmanuel réalise la meilleure performance en entrant dans le top 10 avec sa 9ème place. Younes, Rodrigo et Noah finissent respectivement 14ème, 16ème, et 18ème.

Enfin, en catégorie M9, Amaury ajoute une médaille de bronze au palmarès du RCS en terminant 3ème. 🥉 Maxence conclut ces résultats en terminant 7ème.

Bravo à tous les garçons pour leurs excellentes performances !!



Bravo à Patrick, Arthur et tout le club pour cette performance remarquable !

Remercions également nos arbitres dévoués : Constance, Margot, Yan, Arthur Félix, et Lou 👏

Félicitations à tous !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

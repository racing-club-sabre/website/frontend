---
title: "Championnats de France M13"
date: '2024-06-25'
author: 'Tristan'
cover: '/img/blog/article70-head.jpg'
---
Voici les résultats des Championnats de France chez les M13 🤺

Ce week-end, durant les journées du 22 et 23 juin, s'est déroulée la finale des Championnats de France à Thonon-les-Bains. Nos 12 tireuses et tireurs du RCS chez les M13 ont combattu individuellement et par équipe. Voici les résultats : 

Chez les Dames, Lisa REY-FAYOLLES réalise une performance exceptionnelle en terminant à une magnifique 5ème place en National 1 ! 

En National 2, Hélène OSTERMANN se hisse à la troisième place suivie de Alma PANNETIER à la 20ème place et Méline PORCEL à la 28ème place. 
Enfin, Lili-Rose GACHE termine à la 19ème place en National 3. 

Chez les Garçons c'est Soan ALIN qui réalise la meilleure performance en terminant 16ème en National 1. Adam CHERIF EL FARISSY parvient à une belle 22ème place en National 2. Le reste des garçons ont donc terminé en National 3 au classement suivant : 

- Arthur DURAND 21ème
- Luckas BLANC 27ème 
- Tom CAQUÉ 53ème
- Léo REYMOND 74ème 
- Pierre Emmanuel CARTIGNY 87ème 

Voici les résultats en équipe : 

L'équipe 1 Hommes (Soan, Arthur, Adam, Mathias) termine 3ème en National 2. 

L'équipe 2 Hommes (Luckas, Léo, Tom, Pierre Emmanuel) termine 3ème en National 3.

L'équipe 1 Dames (Lisa, Méline, Hélène, Alma) termine 6ème en National 1. 

Bravo à toutes et à tous ! 

C'était la dernière compétition de la saison pour le RCS, les tireuses et tireurs vont se détendre un peu avant de préparer leur prochaine saison !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

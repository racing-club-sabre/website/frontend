---
title: "Championnats de France UGSEL"
date: '2024-05-31'
author: 'Tristan'
cover: '/img/blog/article66-head.jpg'
---
Le RCS a fait forte impression au championnat UGSEL 2024 avec une participation remarquable.

Yan Boutarin s'impose en tête de la catégorie Lycée Élite Hommes, tandis que Constance Bruyat triomphe chez les Lycée Élite Dames, suivie de Margot en deuxième position.

En Minimes Élites Hommes, Arthur Asmus se distingue en prenant la 2ème place, suivi de Baptiste en 3ème position. Adam, Clément et Yanis terminent respectivement 7ème, 9ème et 12ème.

Chez les Minimes Élites Dames, Agathe décroche l'or pour couronner une très bonne saison. Lou et Elisa prennent les 4ème et 7ème places.

Soan Alin se hisse à la 2ème place en Benjamins Élite, ses camarades figurant parmi les 20 premiers :
- Arthur Durand 6ème
- Léo Reymond 12ème
- Adam 13ème
- Luckas 15ème
- Raphaël 16ème
- Tom Caqué 17ème
- Malo 19ème

Lisa Rey-Fayolle s'empare de la première place chez les Benjamins Élite, suivie de Constance Lanfray (4ème), Alma (5ème), Lili Rose (7ème) et Noeline (8ème).

En Promo Benjamins Hommes, neuf tireurs se sont illustrés :
- 2ème, Thimothée Sébout
- 4ème, Maxence Faure Seriset
- 6ème, Marius Vandernoot
- 7ème, Damien Voloscuk
- 9ème, Kilian Deygas
- 10ème, Loeka Naeyaert
- 12ème, Lheo Aurias
- 15ème, Nael Helies Rulewski
- 18ème, Hugo Marchant

Chez les Promo Benjamines Dames, Ana Archinard et Victoria Chasson montent sur les deuxième et troisième marches du podium, suivies de Lyou Pedriole (4ème), Clara Masson (6ème), Nais Bernal (7ème) et Erine Caqué (10ème).

Les compétitions par équipe ont également été fructueuses pour le RCS. En Benjamines Promo Équipe, Erine et Clara terminent premières, accompagnées par Victoria et Ana à la 3ème place. En Benjamins Promo Équipe, Maxence et Thimothée s'emparent de la première place, suivis de Nael et Loeka (6ème), Lheo et Kilian (7ème), et Marius et Damien (8ème).

Félicitations à tous !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

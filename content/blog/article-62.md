---
title: "Stage de printemps"
date: '2024-04-30'
author: 'Tristan'
cover: '/img/blog/article62-head.jpg'
---
Le RCS Escrime ne connaît pas le repos 💪

Nos Maîtres d’Armes Patrick et Arthur ont organisé un stage pour ces vacances de printemps ! 

Les tireurs du RCS en ont profité pour se détendre et s’améliorer durant 4 jours du Lundi 15 au Jeudi 18 avril 🤺 

De quoi terminer l’année en beauté 😊

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Championnat régional M11 & M15"
date: '2022-10-16'
author: 'Tristan'
cover: '/img/blog/article28-head.jpg'
---
Un futur radieux pour les jeunes du Racing Club Sabre 🤩🤩

Les M11 et les M15 étaient d’attaque pour cette première compétition de la saison au championnat régional à Thonon-Les-Bains ! Et le moins qu’on puisse dire, c’est qu’ils étaient déjà prêts à en découdre avec une pluie de médailles pour le club 🏆

👉 Chez les filles en -11 ans, c’est une vraie razzia : Hélène Ostermann remporte la compétition en battant en finale son équipière Méline Porcel ! Elles sont suivies par Clémence Raucy qui complète le podium, et par Louise Doitrand, 6ème. 🥇🥈🥉

👉 Chez les garçons en -11 ans, même constat avec la victoire d’Adam Cherif El Farissy ! Pierre Emmanuel Cartiny monte sur la 3ème marche du podium, tandis que Maylo Moerman Lebon (11ème), Nassim Amaury (12ème), Malo Thonon Sturbois (14ème) et enfin Célestin Lecoq (15ème) complètent cette délégation Crestoise. 🥇🥉

👉 Chez les filles en -15 ans, Maëlle Bouchet (2ème) et Agathe Mege (3ème) squattent elles aussi le podium, et sont suivies par Lou Brugère (7ème), Méryl Gaydamour (10ème), Soaliah Cros (11ème), Maëlys Defrenne (12ème), Lisa Rey-Fayolle (14ème), Lison Porcel (16ème) et Malia Froment (17ème). 🥈🥉

👉 Enfin, chez les garçons en -15 ans, moins de réussite mais une expérience toujours bonne à prendre pour Vincent Bruyère (10ème), Baptiste Raucy (15ème), Sascha Zawadzki (17ème), Arthus Asmus (23ème), Soan Alin (24ème), Mathyeu Menet (28ème) et Clément Bonini (30ème). 

Quelle superbe performance de la part de tous nos tireurs ! Cela annonce du lourd pour la suite de la saison 😍🔥

Bravo à toutes et tous 👏

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


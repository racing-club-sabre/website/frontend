---
title: "Qualif' aux championnats d'Europe"
date: '2022-04-28'
author: 'Tristan'
cover: '/img/blog/article18-head.jpg'
---

Excellente nouvelle ! 🔥

Suite à ses excellents résultats récents, François Amanrich a été retenu en équipe de France pour les championnats d'Europe à Hambourg 🇩🇪

Une grande fierté pour le club 😍🙌

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

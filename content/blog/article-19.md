---
title: "Photos souvenirs"
date: '2022-03-15'
author: 'Tristan'
cover: '/img/blog/article19-head.jpg'
---
🕰 Récemment, on a décidé de remonter un peu le temps et de fouiller dans les archives ! on est retombé sur quelques pépites… 🤓

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

Vous reconnaissez certains anciens ? N’hésitez pas à les taguer sur la publication 😏

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

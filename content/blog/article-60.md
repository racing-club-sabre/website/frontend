---
title: "1/2 finale M15 Montargis"
date: '2024-03-24'
author: 'Tristan'
cover: '/img/blog/article60-head.jpg'
---
Elle est grande, elle est immense Agathe Mège ! Notre sabreuse monte sur le podium lors de la demi-finale des Championnats de France M15 à Montargis 🤩🥉

👉 Au classement général, elle est suivie par Lou Brugère qui signe une belle 25ème place, puis par Lisa Rey-Fayolle (41ème), Anouck Bouchet (63ème), Lison Porcel (71ème), Elisa Brun (95ème), Meline Porcel (98ème), Alma Pannetier (110ème), et enfin Jeanne Deman-Lemaître (112ème). 

👉 Chez les garçons, Baptiste Raucy se hisse jusqu’aux 8èmes de finale et termine à la 14ème place ! Il est suivi par Arthur Asmus (49ème), Soan Alin (71ème), Hector Nougaret (100ème), Yanis Azzolin (126ème), Adam Tavares (162ème) et Arthur Durand (170ème).

Le rendez-vous est désormais pris pour la Fête des Jeunes 🏆🇫🇷

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

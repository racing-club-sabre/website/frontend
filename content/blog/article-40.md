---
title: "Championnats de France M17"
date: '2023-05-14'
author: 'Tristan'
cover: '/img/blog/article40-head.jpg'
---
Les filles médaillées de bronze N2 aux championnats de France des -17 ans ! 🤩🥉

C’était le week-end dernier à Montargis, nos M17 étaient sur le pont en individuels et par équipe, et ramènent une médaille de cette escapade 🥰👏

L’équipe des garçons, quant à elle, termine à la 7ème place en N3 👊 

Le résultat de la compétition individuelle du samedi : 

🤺 Chez les filles, en première division seule Constance Bruyat était présente : elle termine 18ème meilleure française 🔥 En N2, Maëlle Bouchet et Agathe Mège atteignent les quarts de finale et terminent 6ème et 7ème, tandis que Margot Porcel récupère la 16ème place 

🤺 Chez les garçons, tous les 4 qualifiés en deuxième division, Arthur Félix termine 44ème, Théo Guichard 65ème, Yan Boutarin 68ème et Vincent Bruyère 78ème 

Bravo à tous pour ces performances, et à l’année prochaine sur les pistes 💛🖤

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


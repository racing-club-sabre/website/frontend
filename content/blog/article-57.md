---
title: "Régional M17 Lyon"
date: '2024-02-10'
author: 'Tristan'
cover: '/img/blog/article57-head.jpg'
---
Podium 100% RCS Escrime lors de la dernière compétition régionale des M17 chez les filles 🤩

A Lyon, nos tireuses se sont brillamment imposées face à la concurrence et terminent 1ère (Constance Bruyat), 2ème (Agathe Mege) et 3ème (Maëlle Bouchet) ! 🥇🥈🥉

Elles sont suivies par les soeurs Porcel, Lison et Margot, qui terminent respectivement 6ème et 7ème ! 

Félicitations à elles 💪

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

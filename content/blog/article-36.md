---
title: "Stage des vacances de février"
date: '2023-02-15'
author: 'Tristan'
cover: '/img/blog/article35-head.jpg'
---
Pendant les vacances de février, nos tireurs se sont préparés pour les échéances de fin de saison ! 👊

Ils étaient 22 tireurs présents et surmotivés, avec au programme : échauffement, fondamentaux, assauts, travail technique, poules à thème, rencontres par équipe et le plus important… le repas commun 🤤

Pas de quoi s’ennuyer, donc 😁

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Semaine strasbourgeoise pour nos maîtres d'armes"
date: '2021-10-31'
author: 'Tristan L'
cover: '/img/blog/article8-head.jpg'
---
Semaine sportive à Strasbourg pour nos maîtres d’armes ! 🤺

Le maître Patrick Del Monaco, sélecteur national des -17 ans, était à Strasbourg la semaine passée pour le stage national des M17 et M20, du 25 au 29 octobre, lors duquel il a participé comme entraîneur. 

Ce stage précédait le deuxième circuit national séniors de l’année, toujours en Alsace. Patrick a donc été rejoint pour ce week-end de compétition par son fils Arthur, venu arbitrer dans le cadre de sa formation pour devenir arbitre national. Et quel style !

En prime, une photo souvenir avec les deux médaillées olympiques et finalistes de l’épreuve de ce week-end : Manon Brunet et Cécilia Berder !

![Patrick et Arthur avec les jeunes M17, Brunet et Berder](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article8-text.jpg)

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Zone sud-est Géménos"
date: '2021-11-20'
author: 'Tristan L'
cover: '/img/blog/article10-head.jpg'
---
Zone Sud-Est à Géménos 🤺

Retour sur les résultats du club à la compétition inter-régionale de la semaine dernière ! Ce long weekend d’escrime a vu nos tireurs de M15, M17 et Seniors s’affronter en région marseillaise pour un bon moment partagé tous ensemble, et un premier retour en compétition pour certains d’entre eux !

Chez les filles -15 ans d’abord, c’est Constance Bruyat qui continue sur sa lancée et obtient une belle 5ème place ! Elle est suivie de près par Agathe Mège (6ème) et Maëlle Boucher (8ème), ce qui a placé 3 tireuses du RCS en quart de finale ! On retrouve à la suite du classement Méryl Gaydamour en 10ème position, puis Inaya Lambert (11ème), Soaliah Cros (14ème), Leïla Spaak (17ème), et enfin Maëlys Defrenne (18ème). 

Pour les garçons de la même catégorie, l’apprentissage continue pour nos jeunes, dont nombre d’entre eux étaient encore surclassés. C’est Théo Guichard qui récupère la meilleure place après une défaite en huitième et une 13ème place au final. Derrière lui suivent Vincent Bruyère (22ème), Nino Besançon (27ème), Arthur Asmus (29ème), Ethan Thivolle (32ème), Sascha Zawadzki (33ème), Kayan Bruno (34ème), et enfin Baptiste Raucy qui termine à la 38ème position. 

![Les jeunes tireurs du RCS pendant le stage](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article10-text.jpg)

En M17, nos trois tireurs engagés (Basile Chandon, Arthur Félix et Yan Boutarin) terminent respectivement à la 17ème, 18ème et 21ème position. 

Et pour terminer, chez nos Séniors représentés par nos chers Antonin Roche et Thybaut Espinas, c’est Antonin qui signe une belle performance avec sa 5ème place (3ème M20 !), suivi par Thybaut, 13ème ! 

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

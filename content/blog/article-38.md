---
title: "Stage vacances de Pâques"
date: '2023-04-20'
author: 'Tristan'
cover: '/img/blog/article38-head.jpg'
---
Le stage des vacances de Pâques pour préparer les échéances de fin d’année 😁

Ils étaient 23 tireurs au gymnase Chareyre, du 17 au 20 avril, à travailler pour performer au maximum sur les dernières compétitions de la saison 💪

Ils ont donc bien mérité le dessert de François… 🍓

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


---
title: "Mardi de l’équipe de France"
date: '2021-09-22'
author: 'Tristan L'
cover: '/img/blog/article3-head.jpg'
---
**Mardi de l’équipe de France** 🇫🇷

Nous y étions ! Mardi 21/09, nous avons eu le plaisir d’accueillir **Margaux Rifkiss**, multi-médaillée au niveau international, pensionnaire de [l’INSEP](https://www.insep.fr/fr) et membre de l’équipe de France de sabre féminin.

Au sein de l’[Etablissement Saint-Louis](https://www.stlouis26.eu/), ce fut une très belle journée pour les jeunes élèves qui ont pu découvrir et écouter l’expérience indiscutable de cette grande tireuse ! De son parcours depuis son plus jeune âge jusqu’à ses objectifs pour les [Jeux de Paris 2024](https://www.paris2024.org/fr/), ce fut une superbe intervention pour la numéro 5 française qui nous a fait l’honneur de nous rendre visite. 

![Margaux Rifkiss au milieu des élèves de Saint-Louis](https://gitlab.com/racing-club-sabre/website/frontend/-/raw/master/static/img/blog/article3-text.jpg)

Cet événement a également été l’occasion pour tous les jeunes et de partager un moment de convivialité sur les thèmes _"découverte de l’escrime"_ et _"sport-études"_, le double projet du club avec Saint-Louis pour développer l’escrime et le haut niveau dans la vallée de la Drôme !

Pour plus d’informations sur cette journée organisée par la FFE 👉 https://bit.ly/3EH4HaB

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Circuit national M17 Tarbes"
date: '2024-03-10'
author: 'Tristan'
cover: '/img/blog/article59-head.jpg'
---
Retour sur le Circuit National de Tarbes pour les -17 ans du RCS Escrime, avec les compétitions individuelles le samedi, et la demi-finale des Championnats de France par équipes le dimanche 💥

👉 Chez les filles, Constance Bruyat termine à une superbe 12ème place et confirme ses ambitions pour les prochains championnats de France 👊
Elle est suivie par Maëlle Bouchet qui échoue aux portes des 16èmes de finale en terminant 33ème, puis par Agathe Mège (54ème) qui continue son apprentissage à ce niveau. Lou Brugère (58ème) et Margot Porcel (77ème) ferment la marche !

👉 Chez les garçons, Arthur Asmus termine 61ème, tandis que Baptiste Raucy récupère la 74ème place. 

👉 Le dimanche, l’équipe féminine du RCS atteint son objectif en se qualifiant pour les Championnats de France à Vannes, avec une belle 7ème place ! Les filles se battront donc pour une médaille en première ou deuxième division en Bretagne 🥳

Et, en guise de surprise, nos tireurs ont eu la chance de croiser Maxime Pianfetti, vice-champion du monde de sabre en 2022 😍

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Stage vacances de février"
date: '2022-02-18'
author: 'Tristan'
cover: '/img/blog/article14-head.jpg'
---

Différentes générations en stage ! 😁🤺

En cette semaine de vacances scolaires, Arthur et Patrick en ont profité pour organiser un stage afin de préparer au mieux les échéances à venir 💪

👉 **Plus de 20 jeunes se sont donc retrouvés** du 14 au 16 février lors au gymnase Chareyre !

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

---
title: "Animation club Noël"
date: '2023-12-12'
author: 'Tristan'
cover: '/img/blog/article54-head.jpg'
---
Retour en images sur l’animation club du RCS Escrime avant Noël 📸🎄

L’occasion pour petits et grands de s’affronter dans une ambiance festive et conviviale 🥰

Et vous, quels sont vos meilleurs souvenirs d’animations du club ? 🤩

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


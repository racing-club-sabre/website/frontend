---
title: "Challenge de France 2022"
date: '2022-06-28'
author: 'Tristan'
cover: '/img/blog/article23-head.jpg'
---
Après la Fête des jeunes, place au Challenge de France ! 🔥

Cette fois-ci, c’était les -13 ans qui se déplaçaient à Dinard pour les championnats de France de leur catégorie, une première superbe expérience au niveau national pour nombre d’entre eux 🥰

👉 Les filles par équipe montent notamment sur le podium avec une magnifique 2ème place, et régalent également en individuel avec la 5ème place d’Agathe Mege, la 12ème de Soaliah Cros et la 23ème d’Anouk Bouchet 🤩

👉 Chez les garçons, en N1, Baptiste Raucy termine 17ème et est suivi par Arthur Asmus (40ème). En N2, Sascha Zawadzki termine aussi à la deuxième place, et en N3 Soan Alin finit à la 24ème position 💪

Félicitations à nos jeunes pousses 😍😍

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


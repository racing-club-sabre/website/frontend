---
title: "Forums de Livron & Beaumont-lès-Valence"
date: '2023-09-05'
author: 'Tristan'
cover: '/img/blog/article47-head.jpg'
---
Quelques photos de notre présence sur les forums de Livron et de Beaumont-lès-Valence en ce début d’année ! 😁

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


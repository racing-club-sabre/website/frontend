---
title: "Zone M17 & Seniors à Meylan"
date: '2022-11-19'
author: 'Tristan'
cover: '/img/blog/article31-head.jpg'
---
Razzia de médailles chez les M17 pour la compétition de zone Sud-Est à Meylan ! 🏆🔥

👉 Constance Bruyat reprend goût aux médailles d’or 🥇 en venant à bout de sa coéquipière Maëlle Bouchet en finale 🥈, sous les yeux de sa grande soeur Manon 👀
Elles sont suivies par Agathe Mège, 6ème, et Margot Porcel, 11ème ! 

👉 Chez les garçons, Yan Boutarin monte aussi sur la boîte avec sa belle 3ème place 🥉, et est suivi par Nino Besançon (8ème) qui se hisse jusqu’en quart de finale, puis Arthur Félix (12ème), Vincent Bruyère (16ème), Théo Guichard (20ème) et Baptiste Raucy (24ème). 

Félicitations à toutes et tous, et à très vite sur les pistes 😁

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


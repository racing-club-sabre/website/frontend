---
title: "Compétition universitaire Lyon"
date: '2021-12-03'
author: 'Tristan L'
cover: '/img/blog/article12-head.jpg'
---
Retour sur la performance de nos étudiants et escrimeurs à la première compétition universitaire de l’année qui avait lieu à Lyon.

Un plaisir de revoir ces visages radieux ! Bravo à Manon Bruyat et Lauriane Lebeau pour leurs 2ème et 3ème places, ainsi qu’à Martin Huraut qui termine 12ème.

A bientôt pour la suite 🤺

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

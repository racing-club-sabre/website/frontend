---
title: "Fête des jeunes 2022"
date: '2022-06-20'
author: 'Tristan'
cover: '/img/blog/article22-head.jpg'
---
Retour sur les résultats de la Fête des jeunes pour les filles du Racing Club Sabre ! 🤩🇫🇷

🥈 Constance Bruyat médaillée d’argent par équipe ! Intégrée à l’équipe 1 AuRA, elle monte sur la deuxième place du podium en N1 avec ses coéquipières lors d’une journée vive en émotion ✨

En individuel, Constance termine à la 24ème place, et elle est suivie par Maëlle Bouchet qui accroche une belle 32ème place, puis par Méryl Gaydamour (51ème), Agathe Mege (52ème), Sohalia Cros (64ème), Maëlys Defrenne (73ème), Anouk Bouchet (76ème) et enfin Inaya Lambert (80ème).   👏

Une superbe expérience au niveau national pour les filles, bravo à elles 😍

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !

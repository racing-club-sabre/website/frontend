---
title: "Reprise de la saison 2022-2023"
date: '2022-08-28'
author: 'Tristan'
cover: '/img/blog/article26-head.jpg'
---
🔥 Reprise des cours d’escrime 🔥

Le Racing Club Sabre et ses élèves étaient ravis de se remettre en piste pour attaquer la nouvelle saison ! 😍

A cette occasion, un stage de pré-rentrée était organisé au gymnase Chareyre, pour le plus grand bonheur des petits & des grands 🥰

Et pour tout le monde : les inscriptions pour cette saison sont ouvertes ! 🤓✍️

👉 Rendez-vous sur ce lien pour vous inscrire en ligne : https://bit.ly/3cEUnXu

👉 Rendez-vous sur ce lien pour vous inscrire via la fiche d’inscription (à retourner par mail à Patrick Del Monaco) : https://bit.ly/3wORXfD

👉 Rendez-vous sur ce lien pour télécharger vos certificats médicaux : https://bit.ly/3q01D3a

On a hâte de vous retrouver à la salle d’armes 🤺

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


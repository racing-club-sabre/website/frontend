---
title: "Circuit national M20 Strasbourg"
date: '2023-10-21'
author: 'Tristan'
cover: '/img/blog/article49-head.jpg'
---
Retour sur le circuit national M20 de Strasbourg du 21 octobre dernier ! 🤺

Résultats en dents de scie pour nos tireurs, qui continuent d’apprendre et d’engranger de l’expérience malgré tout 💪

Chez les dames, Constance Bruyat termine à la 42ème place, tandis que chez les hommes, c’est Yan Boutarin qui fait le meilleur résultat grâce à sa 89ème place. Il est suivi par Arthur Félix (111ème) et Corentin Veillard (113ème). 

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Beaumont-lès-Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


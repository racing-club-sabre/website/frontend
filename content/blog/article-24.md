---
title: "Arthur nommé arbitre national de sabre !"
date: '2022-07-07'
author: 'Tristan'
cover: '/img/blog/article24-head.jpg'
---
🤩 Arthur nommé arbitre national au sabre 🤩

Ça y est, c’est officiel ! Notre cher Arthur s’est vu attribuer le rang d’arbitre national après avoir passé avec succès les différentes étapes nécessaires à son attribution (théorie et pratique) ! Un très grand bravo à lui 🔥👏

🚀 Félicitations également à nos jeunes Vincent, Maëlle, Méryl, Nino, Yan, Théo, Arthur et Baptiste qui ont eux validé leur QCM départemental et pourront dès la rentrée prochaine tenter de devenir arbitres départementaux !

Parce qu’il faut aussi des arbitres de qualité lors d’un assaut d’escrime, le RCS s’engage dans cette voie 🤺

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


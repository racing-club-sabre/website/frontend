---
title: "Championnats de France UGSEL"
date: '2023-06-08'
author: 'Tristan'
cover: '/img/blog/article43-head.jpg'
---
Retour sur les Championnats de France UGSEL du mois de juin ! 💥
 
Une nouvelle fois présent en force, le RCS Escrime ramène six nouvelles breloques de cette compétition 🤩 Tous les résultats : 

👉 Catégorie Benjamin Promotionnel homme
1er Arthur DURAND 🏆
2ème Tom CAQUE 🥈
3ème Léo Jules REYMOND 🥉
4ème Marius THOMAS
15ème Luckas BLANC

👉 Catégorie Benjamine Promotionnel Dame
2ème Jeanne DEMAN LEMAITRE 🥈
4ème Alma PANNETIER
6ème Noéline SEIGNOBOS
10ème Lili Rose GACHE
16ème Liloa RIBEIRO

👉 Catégorie Benjamine élite
1er Lisa REY--FAYOLLE 🏆
6ème Elisa BRUN

👉 Catégorie Benjamin élite
6ème Soan ALIN
10ème Adam TAVARES
12ème Yanis AZZOLIN
13ème Clément BRUN

👉 Catégorie Minime dame
3ème Agathe MEGE 🥉
6ème Lou BRUGERE
8ème Maëlys DEFRENNE
13ème Sohalia CROS
16ème Malia FROMENT
18ème Lana DELPLANCQ

👉 Catégorie Minime homme
4ème Arthur ASMUS
6ème Baptiste RAUCY
7ème Vincent BRUYERE
14ème Sasha ZAWADSKIS

👉 Catégorie Lycée dame
5ème Constance BRUYAT
7ème Margot PORCEL

👉 Catégorie Lycée homme
4ème Yan BOUTARIN
11ème Corentin VEILLARD


Félicitations à tout le monde, et à très vite sur les pistes pour la rentrée ! 🤺🔥

Toutes les photos sont à retrouver ici 👉 https://www.facebook.com/racingclubsabre

______________________

Le Racing Club Sabre est un club d’escrime spécialisé dans le sabre, présent au coeur de la vallée de la Drôme dans les villes de Crest, Livron et Valence. Rejoignez-nous sur [Facebook](https://www.facebook.com/racingclubsabre) et [Instagram](https://www.instagram.com/rcs_escrime/) !


import colors from 'vuetify/es5/util/colors'
import Vue from 'vue'
import fr from 'vuetify/es5/locale/fr'

export default {
  target: 'static',
  telemetry: false,
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate(titleChunk) {
      return titleChunk
        ? `${titleChunk} − Racing Club Sabre`
        : 'Racing Club Sabre'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/png', href: '/img/logo_big.png' }],
  },
  messages: {
    loading: 'Chargement...',
    error_404: 'Page introuvable',
    server_error: 'Erreur du serveur',
    nuxtjs: 'Nuxt.js',
    back_to_home: "Retour à l'accueil",
    server_error_details:
      "Une erreur est survenue et la page n'a pas pu être affichée. Si vous avez accès au serveur, consultez les logs pour plus de détails.",
    client_error: 'Erreur',
    client_error_details:
      'Une erreur est survenue au chargement de la page. Consultez la console (F12) pour plus de détails.',
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** env
   */
  env: {
    mapboxToken: process.env.MAPBOX_TOKEN,
  },
  /*
   ** Global CSS
   */
  css: ['~/assets/main.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/vuetify'],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxt/content'],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    lang: {
      locales: { fr },
      current: 'fr',
    },
    defaultAssets: false,
    icons: {
      iconfont: 'mdiSvg',
    },
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: '#00bcd4',
          secondary: '#673ab7',
          accent: '#607d8b',
          error: '#f44336',
          warning: '#ff9800',
          info: '#2196f3',
          success: '#4caf50',
        },
      },
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
  generate: {
    dir: 'public',
    fallback: true,
  },
}

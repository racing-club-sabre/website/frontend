export const state = () => ({
  rooms: {
    Crest: {
      name: 'Crest',
      location: {
        number: '10',
        line1: 'Quai Berangier de la Blache',
        postcode: '26400',
        suburb: 'Crest',
        state: 'Auvergne-Rhône-Alpes',
        country: 'France',
        geo: [5.02507, 44.72666],
      },
    },
    Livron: {
      name: 'Livron',
      location: {
        name: 'Gymnase Claude Bon',
        number: 2,
        line1: 'Rue de la Sablière',
        postcode: '26250',
        suburb: 'Livron-sur-Drôme',
        state: 'Auvergne-Rhône-Alpes',
        country: 'France',
        geo: [4.84178, 44.7761],
      },
    },
    'Beaumont-lès-Valence': {
      name: 'Beaumont-lès-Valence',
      location: {
        name: 'Gymnase Marcel Cerdan',
        number: 6,
        line1: 'Chemin des Fontaines',
        postcode: '26760',
        suburb: 'Beaumont-lès-Valence',
        state: 'Auvergne-Rhône-Alpes',
        country: 'France',
        geo: [4.93951, 44.86496],
      },
    },
  },

  activities: {
    escrime: {
      name: 'Escrime',
      categories: [
        {
          name: 'M7',
          ageLowerLimit: 6,
          ageUpperLimit: 7,
          licenceCost: 22.78,
        },
        {
          name: 'M5',
          ageLowerLimit: 0,
          ageUpperLimit: 5,
          licenceCost: 22.78,
        },
        {
          name: 'M9',
          ageLowerLimit: 8,
          ageUpperLimit: 9,
          licenceCost: 42.78,
        },
        {
          name: 'M11',
          ageLowerLimit: 10,
          ageUpperLimit: 11,
          licenceCost: 57.28,
        },
        {
          name: 'M13',
          ageLowerLimit: 12,
          ageUpperLimit: 13,
          licenceCost: 57.28,
        },
        {
          name: 'M15',
          ageLowerLimit: 14,
          ageUpperLimit: 15,
          licenceCost: 57.28,
          needsCompetitionFee: true,
        },
        {
          name: 'M17',
          ageLowerLimit: 16,
          ageUpperLimit: 17,
          licenceCost: 57.28,
          needsCompetitionFee: true,
        },
        {
          name: 'M20',
          ageLowerLimit: 18,
          ageUpperLimit: 20,
          licenceCost: 57.28,
          needsCompetitionFee: true,
        },
        {
          name: 'Sénior',
          ageLowerLimit: 21,
          ageUpperLimit: 40,
          licenceCost: 57.28,
          needsCompetitionFee: true,
        },
        {
          name: 'Vétéran',
          ageLowerLimit: 41,
          licenceCost: 57.28,
          needsCompetitionFee: true,
        },
      ],
      trainings: [
        {
          day: 'monday',
          room: 'Livron',
          startTime: 17,
          duration: 60,
          indicativeMinAge: 5,
          indicativeMaxAge: 7,
        },
        {
          day: 'monday',
          room: 'Livron',
          startTime: 18,
          duration: 60,
          indicativeMinAge: 8,
          indicativeMaxAge: 10,
        },
        {
          day: 'monday',
          room: 'Livron',
          startTime: 19,
          duration: 90,
          indicativeMinAge: 11,
        },
        {
          day: 'tuesday',
          room: 'Beaumont-lès-Valence',
          startTime: 16.75,
          duration: 90,
          indicativeMinAge: 7,
          indicativeMaxAge: 10,
        },
        {
          day: 'tuesday',
          room: 'Crest',
          startTime: 17.5,
          duration: 90,
          indicativeMinAge: 11,
          indicativeMaxAge: 13,
        },
        {
          day: 'tuesday',
          room: 'Crest',
          startTime: 19,
          duration: 90,
          indicativeMinAge: 14,
        },
        {
          day: 'wednesday',
          room: 'Crest',
          startTime: 16,
          duration: 60,
          indicativeMinAge: 5,
          indicativeMaxAge: 7,
        },
        {
          day: 'wednesday',
          room: 'Crest',
          startTime: 17,
          duration: 90,
          indicativeMinAge: 8,
          indicativeMaxAge: 10,
        },
        {
          day: 'wednesday',
          room: 'Crest',
          startTime: 20,
          duration: 90,
          comment: 'Adultes Loisirs',
        },
        {
          day: 'thursday',
          room: 'Beaumont-lès-Valence',
          startTime: 16.75,
          duration: 90,
          indicativeMinAge: 11,
          indicativeMaxAge: 14,
        },
        {
          day: 'friday',
          room: 'Crest',
          startTime: 17.5,
          duration: 90,
          indicativeMinAge: 11,
          indicativeMaxAge: 13,
        },
        {
          day: 'friday',
          room: 'Crest',
          startTime: 19,
          duration: 90,
          indicativeMinAge: 14,
        },
      ],
      maxTrainings: 3,
      needsCertificate: true,
      needsLicence: true,
      requires: 'member',
    },
    gym: {
      name: "Gymnastique d'entretien",
      categories: [{ name: 'Tous' }],
      maxTrainings: 1,
      needsCertificate: true,
      requires: 'member',
    },
    member: {
      name: 'Adhésion',
      categories: [{ name: 'Tous' }],
    },
    escrimeStLouis: {
      name: 'Horaires aménagés St-Louis',
      trainings: [
        {
          day: 'tuesday',
          room: 'Crest',
          startTime: 16,
          duration: 90,
          comment: '6ème',
        },
        {
          day: 'thursday',
          room: 'Crest',
          startTime: 16,
          duration: 90,
          comment: '5ème à 3ème',
        },
        {
          day: 'friday',
          room: 'Crest',
          startTime: 15,
          duration: 90,
          comment: 'CM1 & CM2',
        },
      ],
      categories: [
        {
          name: 'M7',
          ageLowerLimit: 6,
          ageUpperLimit: 7,
        },
        {
          name: 'M5',
          ageLowerLimit: 0,
          ageUpperLimit: 5,
        },
        {
          name: 'M9',
          ageLowerLimit: 8,
          ageUpperLimit: 9,
        },
        {
          name: 'M11',
          ageLowerLimit: 10,
          ageUpperLimit: 11,
        },
        {
          name: 'M13',
          ageLowerLimit: 12,
          ageUpperLimit: 13,
        },
        {
          name: 'M15',
          ageLowerLimit: 14,
          ageUpperLimit: 15,
        },
        {
          name: 'M17',
          ageLowerLimit: 16,
          ageUpperLimit: 17,
        },
        {
          name: 'M20',
          ageLowerLimit: 18,
          ageUpperLimit: 20,
        },
        {
          name: 'Sénior',
          ageLowerLimit: 21,
          ageUpperLimit: 40,
        },
        {
          name: 'Vétéran',
          ageLowerLimit: 41,
        },
      ],
      maxTrainings: 1,
      needsCertificate: true,
      requires: 'member',
    },
    sportEtude: {
      name: 'Classe Sport Étude St-Louis',
      trainings: [
        {
          day: 'tuesday',
          room: 'Crest',
          startTime: 19,
          duration: 90,
        },
        {
          day: 'wednesday',
          room: 'Crest',
          startTime: 13.5,
          duration: 120,
        },
        {
          day: 'friday',
          room: 'Crest',
          startTime: 19,
          duration: 90,
        },
      ],
      categories: [
        {
          name: 'M7',
          ageLowerLimit: 6,
          ageUpperLimit: 7,
        },
        {
          name: 'M5',
          ageLowerLimit: 0,
          ageUpperLimit: 5,
        },
        {
          name: 'M9',
          ageLowerLimit: 8,
          ageUpperLimit: 9,
        },
        {
          name: 'M11',
          ageLowerLimit: 10,
          ageUpperLimit: 11,
        },
        {
          name: 'M13',
          ageLowerLimit: 12,
          ageUpperLimit: 13,
        },
        {
          name: 'M15',
          ageLowerLimit: 14,
          ageUpperLimit: 15,
        },
        {
          name: 'M17',
          ageLowerLimit: 16,
          ageUpperLimit: 17,
        },
        {
          name: 'M20',
          ageLowerLimit: 18,
          ageUpperLimit: 20,
        },
        {
          name: 'Sénior',
          ageLowerLimit: 21,
          ageUpperLimit: 40,
        },
        {
          name: 'Vétéran',
          ageLowerLimit: 41,
        },
      ],
      maxTrainings: 1,
      needsCertificate: true,
      requires: 'member',
    },
  },
})

export const getters = {
  currentSeason() {
    let year = new Date().getFullYear()
    if (new Date().getMonth() >= 6) {
      year++
    }
    return year
  },
}
